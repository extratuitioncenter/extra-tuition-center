import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Main {

	public static void main(String args[]) throws Exception {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ArrayList<Tutor> tutorsList = DataUtil.getTutors();
		ArrayList<Student> studentList = DataUtil.getStudents();

		// sample data
		Tutor tut = new Tutor("test");
		ArrayList<String> subs = new ArrayList<String>();
		subs.add("english");
		tut.setSubjectList(subs);
		tutorsList.add(tut);

		Student stud = new Student("Student A", "Male", "1-1-1", "test", "test");
		stud.setMainTutor("Test");
		studentList.add(stud);

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		System.out.println("---------- Extra Tuition Centre ----------\n");
		boolean mainLoop = true;
		// reading the user's choice and process accordingly
		while (mainLoop) {

			try {
				System.out.println("Choose an option to Continue: \n");
				System.out.println("1. Tutor\n2. Student\n3. Book Supply\n4. Report\n5. Exit\n");
				Integer choice = Integer.parseInt(br.readLine());

				switch (choice) {
				case 1:
					System.out.println("\nInput your choice\n1. Add Tutor\n2. Handle Class\n3. Back");
					Integer tutorsChoice = Integer.parseInt(br.readLine());
					switch (tutorsChoice) {
					case 1:
						System.out.print("Enter the tutor name: ");
						String tutorName = br.readLine();
						System.out.println("Enter the number of subject");
						Integer subjectsCount = Integer.parseInt(br.readLine());
						Tutor tutor = new Tutor(tutorName.toLowerCase());
						ArrayList<String> subjects = new ArrayList<String>();
						for (int i = 0; i < subjectsCount; i++) {
							System.out.print("Enter the subject " + (i + 1) + " : ");
							String subject = br.readLine().toLowerCase();
							subjects.add(subject);
						}
						tutor.setSubjectList(subjects);
						tutorsList.add(tutor);
						System.out.println("Tutor Added Successfully!");
						break;
					case 2:
						System.out.println("Enter tutor name: ");
						String tutorsName = br.readLine().toLowerCase();
						boolean available = false;
						Tutor tutors = null;
						for (Tutor tutor1 : tutorsList) {
							if (tutor1.getName().equalsIgnoreCase(tutorsName)) {
								available = true;
								tutors = tutor1;
							}
						}
						if (available) {
							System.out.println("Choose a lesson to proceed: ");
							System.out.println("The booked lessons are: ");
							int j = 0;

							for (Lesson s : tutors.getBookedLessons()) {
								if (s.getStatus().equalsIgnoreCase("Available")) {
									System.out.println((j + 1) + ". " + s.getSubject());
								}
								j++;
							}

							Integer updateLesson = Integer.parseInt(br.readLine());
							Lesson lesson = tutors.getBookedLessons().get(updateLesson - 1);
							Student student = lesson.getStudent();
							System.out.println("Enter notes: ");
							String notes = br.readLine();
							student.setNotes(student.getNotes() + "\n" + notes);
							System.out.println("Enter the number of books: ");
							Integer bookCount = Integer.parseInt(br.readLine());

							ArrayList<Book> bookList = new ArrayList<Book>(student.getRequestedBooksList());

							for (int u = 0; u < bookCount; u++) {
								System.out.println("Enter book name: ");
								String bookName = br.readLine();
								System.out.println("Enter book count: ");
								Integer booksCount = Integer.parseInt(br.readLine());
								bookList.add(new Book(bookName, booksCount));
							}

							student.setRequestedBooksList(bookList);
							lesson.setStudent(student);
							tutors.setLesson(lesson);

							int m = 0;
							for (Tutor i : tutorsList) {
								if (i.getName().equalsIgnoreCase(tutors.getName())) {
									break;
								}
								m++;
							}

							int n = 0;
							for (Student ss : studentList) {
								if (ss.getName().equalsIgnoreCase(student.getName())) {
									break;
								}
								n++;
							}

							tutorsList.set(m, tutors);
							studentList.set(n, student);
							System.out.println("Updated Successfully!");

						} else {
							System.out.println("Tutor not found!!");
						}
						break;
					default:
						break;
					}

					break;
				case 2:

					System.out.println("\nInput your choice\n1. Register\n2. Login\n3. Back");
					Integer studentChoice = Integer.parseInt(br.readLine());
					switch (studentChoice) {

					case 1:
						System.out.println("Please enter the following details:");
						System.out.print("Student Name: ");
						String studentName = br.readLine();
						System.out.print("Gender: ");
						String gender = br.readLine();
						System.out.print("Date of Birth: ");
						String dob = br.readLine();
						System.out.print("Address: ");
						String address = br.readLine();
						System.out.print("Phone Number: ");
						String phone = br.readLine();

						Student student = new Student(studentName, gender, dob, address, phone);

						while (true) {
							System.out.println("Choose your main tutor: \nThe available Tutors are: ");
							int i = 0;

							for (Tutor currentTutor : tutorsList) {
								System.out.println((i + 1) + ". " + currentTutor.getName().toUpperCase());
								i++;
							}

							Integer tutorChoice = Integer.parseInt(br.readLine());
							if (tutorChoice <= tutorsList.size()) {
								student.setMainTutor(tutorsList.get(tutorChoice - 1).getName());
								break;
							} else {
								continue;
							}
						}

						studentList.add(student);
						System.out.println("Student Registered Successfully!\n");
						break;

					case 2:
						System.out.print("Enter the Student Name: ");
						String loginName = br.readLine();
						Integer indexOfCurrentStudent = 0, indexOfCurrentTutor = 0;
						boolean loop2 = true;
						for (Student currentStudent : studentList) {

							if (currentStudent.getName().equalsIgnoreCase(loginName)) {
								while (loop2) {
									System.out.println(
											"Choose an option to continue: \n1. Request Lesson\n2. Update Lesson\n3. Cancel\n4. Attend\n5. Failed\n6. Back\n");
									Integer opinion = Integer.parseInt(br.readLine());
									if (opinion == 1) {

										System.out.println("Enter the subject name: ");
										String nameOfSubject = br.readLine();
										System.out.println("Enter the subject description: ");
										String description = br.readLine();
										System.out.println("Enter the date(dd-MM-YYYY): ");
										Date date = sdf.parse(br.readLine());

										Tutor subjectTutor = null;

										for (Tutor requiredTutor : tutorsList) {
											if (subjectTutor == null) {
												if (requiredTutor.getSubjectList()
														.contains(nameOfSubject.toLowerCase())) {
													if (requiredTutor.getBookedLessons().size() > 0) {
														for (Lesson lesson : requiredTutor.getBookedLessons()) {
															if (lesson.getDate() != null
																	&& date.compareTo(lesson.getDate()) == 0) {
																continue;
															} else {
																subjectTutor = requiredTutor;
															}
														}
													} else {
														subjectTutor = requiredTutor;
													}
												}
											}
											indexOfCurrentTutor++;
										}

										if (subjectTutor != null) {
											Lesson lesson = new Lesson(currentStudent, nameOfSubject, "Available",
													subjectTutor, date, description);
											currentStudent.setLesson(lesson);
											subjectTutor.setLesson(lesson);
											studentList.set(indexOfCurrentStudent, currentStudent);
											tutorsList.set(indexOfCurrentTutor - 1, subjectTutor);
											System.out.println("Lesson requested successfully!");
											break;
										} else {
											System.out.println("Tutor Unavailable!!!");
										}

									} else if (opinion == 2) {

										System.out.println("The available lessons are: ");

										int j = 0;
										for (Lesson lesson : currentStudent.getLessonList()) {
											System.out.println((j + 1) + ". " + lesson.getSubject());
											j++;
										}

										System.out.println("\nChoose to continue: ");
										Integer updateChoice = Integer.parseInt(br.readLine());
										if ((updateChoice - 1) < currentStudent.getLessonList().size()) {

											System.out.println("Enter the subject name: ");
											String nameOfSubject = br.readLine();
											System.out.println("Enter the subject description: ");
											String description = br.readLine();
											System.out.println("Enter the date(dd-MM-YYYY): ");
											Date date = sdf.parse(br.readLine());

											Tutor subjectTutor = null;

											for (Tutor requiredTutor : tutorsList) {
												if (subjectTutor == null) {
													if (requiredTutor.getSubjectList()
															.contains(nameOfSubject.toLowerCase())) {
														if (requiredTutor.getBookedLessons().size() > 0) {
															for (Lesson lesson : requiredTutor.getBookedLessons()) {
																if (lesson.getDate() != null
																		&& date.compareTo(lesson.getDate()) == 0) {
																	continue;
																} else {
																	subjectTutor = requiredTutor;
																}
															}
														} else {
															subjectTutor = requiredTutor;
														}
													}
												}
												indexOfCurrentTutor++;
											}

											if (subjectTutor != null) {
												Lesson lesson = new Lesson(currentStudent, nameOfSubject, "Available",
														subjectTutor, date, description);
												currentStudent.setLesson(lesson, updateChoice);
												subjectTutor.setLesson(lesson, updateChoice);
												studentList.set(indexOfCurrentStudent, currentStudent);
												tutorsList.set(indexOfCurrentTutor - 1, subjectTutor);
												System.out.println("Lesson updated successfully!");
												break;
											} else {
												System.out.println("Tutor Unavailable!!!");
											}

										} else {
											System.out.println("Invalid!!");
										}

									} else if (opinion == 3) {

										System.out.println("The available lessons are: ");

										int j = 0;
										for (Lesson lesson : currentStudent.getLessonList()) {
											if (lesson.getStatus().equalsIgnoreCase("Available")) {
												System.out.println((j + 1) + ". " + lesson.getSubject());
											}
											j++;
										}

										Integer lessonIndex = Integer.parseInt(br.readLine());
										Lesson lesson = currentStudent.getLessonList().get(lessonIndex - 1);
										lesson.setStatus("Cancelled");

										currentStudent.setLesson(lesson, lessonIndex);

										int i = 0;
										for (Student std : studentList) {
											if (std.getName().equalsIgnoreCase(currentStudent.getName())) {
												break;
											}
											i++;
										}

										studentList.set(i, currentStudent);

										int s = 0;
										Tutor tutor1 = null;
										boolean found = false;
										for (Tutor tuts : tutorsList) {
											for (Lesson l : tuts.getBookedLessons()) {

												if (l.getStudent().getName()
														.equalsIgnoreCase(currentStudent.getName())) {
													tutor1 = tuts;
													found = true;
													break;
												}
											}
											if (found) {
												break;
											}
											s++;
										}
										tutorsList.set(s, tutor1);
										System.out.println("Lesson Cancelled");

									} else if (opinion == 4) {

										System.out.println("The available lessons are: ");

										int j = 0;
										for (Lesson lesson : currentStudent.getLessonList()) {
											if (lesson.getStatus().equalsIgnoreCase("Available")) {
												System.out.println((j + 1) + ". " + lesson.getSubject());
											}
											j++;
										}

										Integer lessonIndex = Integer.parseInt(br.readLine());
										Lesson lesson = currentStudent.getLessonList().get(lessonIndex - 1);
										lesson.setStatus("Attended");

										currentStudent.setLesson(lesson, lessonIndex);

										int i = 0;
										for (Student std : studentList) {
											if (std.getName().equalsIgnoreCase(currentStudent.getName())) {
												break;
											}
											i++;
										}

										studentList.set(i, currentStudent);

										int s = 0;
										Tutor tutor1 = null;
										boolean found = false;
										for (Tutor tuts : tutorsList) {
											for (Lesson l : tuts.getBookedLessons()) {

												if (l.getStudent().getName()
														.equalsIgnoreCase(currentStudent.getName())) {
													tutor1 = tuts;
													found = true;
													break;
												}
											}
											if (found) {
												break;
											}
											s++;
										}
										tutorsList.set(s, tutor1);
										System.out.println("Lesson Attended");
										break;
									} else if (opinion == 5) {
										System.out.println("The available lessons are: ");

										int j = 0;
										for (Lesson lesson : currentStudent.getLessonList()) {
											if (lesson.getStatus().equalsIgnoreCase("Available")) {
												System.out.println((j + 1) + ". " + lesson.getSubject());
											}
											j++;
										}

										Integer lessonIndex = Integer.parseInt(br.readLine());
										Lesson lesson = currentStudent.getLessonList().get(lessonIndex - 1);
										lesson.setStatus("Failed");

										currentStudent.setLesson(lesson, lessonIndex);

										int i = 0;
										for (Student std : studentList) {
											if (std.getName().equalsIgnoreCase(currentStudent.getName())) {
												break;
											}
											i++;
										}

										studentList.set(i, currentStudent);

										int s = 0;
										Tutor tutor1 = null;
										boolean found = false;
										for (Tutor tuts : tutorsList) {
											for (Lesson l : tuts.getBookedLessons()) {

												if (l.getStudent().getName()
														.equalsIgnoreCase(currentStudent.getName())) {
													tutor1 = tuts;
													found = true;
													break;
												}
											}
											if (found) {
												break;
											}
											s++;
										}
										tutorsList.set(s, tutor1);
										System.out.println("Lesson Failed to attend");
									} else {
										//System.out.println("Invalid Choice!!\n");
										loop2 = false;
										continue;
									}
								}
							} else {
								// System.out.println("Student not found!!");
								break;
							}
							indexOfCurrentStudent++;
						}
						break;

					default:
						//loop2 = false;
						break;
					}

					break;
				case 3:
					// book
					System.out.println("The requested books: ");
					for (Student students : studentList) {
						if (students.getRequestedBooksList().size() > 0) {
							System.out.println("Student Name: " + students.getName());
							for (Book book : students.getRequestedBooksList()) {
								System.out.println(book.getBookName());
							}
						}
					}

					break;
				case 4:
					// Report
					System.out.println("\n----- Student Details -----");

					for (Student students : studentList) {
						System.out.println("\nStudent Name: " + students.getName());
						System.out.println("Gender: " + students.getGender());
						System.out.println("Date of Birth: " + students.getDob());
						System.out.println("Address" + students.getAddress());
						System.out.println("Phone: " + students.getPhoneNumber());
						for (Book book : students.getRequestedBooksList()) {
							System.out.println(book.getBookName());
						}
					}

					for (Tutor tutor : tutorsList) {
						System.out.println(tutor.getName());
						System.out.println("=== Booked Lessons ===");
						for (Lesson les : tutor.getBookedLessons()) {
							System.out.println("Description: " + les.getDescription());
							System.out.println("Status: " + les.getStatus());
						}
					}

					break;
				case 5:
					System.out.println("Exiting...");
					System.exit(0);
					break;
				default:
					break;
				}

			} catch (Exception e) {
				System.out.println("Exception !!\n");
				e.printStackTrace();
			}
		}
	}

}
