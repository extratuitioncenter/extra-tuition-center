import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.junit.Test;

// unit tests
public class UnitTest {

	@Test
	public void testStudentIndexing() throws Exception{
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		Tutor tutor1 = new Tutor("tutor 1");
		ArrayList<String> subs1 = new ArrayList<String>();
		subs1.add("english");
		tutor1.setSubjectList(subs1);

		Student student1 = new Student("Student 1", "Male", "1-1-1989", "George Street", "325611");
		student1.setMainTutor("tutor 1");
		ArrayList<Book> bookList1 = new ArrayList<Book>();
		bookList1.add(new Book("Mental Math", 1));
		bookList1.add(new Book("English Comprehension", 3));
		student1.setRequestedBooksList(bookList1);
		Student tempStd = student1;
		Lesson lesson1 = new Lesson(tempStd, "English", "Available", tutor1, sdf.parse("1-4-2015"), "Communication");
		student1.setLesson(lesson1);
		
		Lesson lesson2 = new Lesson(tempStd, "English", "Available", tutor1, sdf.parse("1-4-2005"), "Skills");
		String description1 = lesson2.getDescription();
		
		// updating the lesson and testing
		
		student1.setLesson(lesson2, 1);

		assertEquals(description1, student1.getLessonList().get(0).getDescription());
	}
	
	
	@Test
	public void testTutorIndexing() throws Exception{
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		Tutor tutor1 = new Tutor("tutor 1");
		ArrayList<String> subs1 = new ArrayList<String>();
		subs1.add("english");
		tutor1.setSubjectList(subs1);

		Student student1 = new Student("Student 1", "Male", "1-1-1989", "George Street", "325611");
		student1.setMainTutor("tutor 1");
		ArrayList<Book> bookList1 = new ArrayList<Book>();
		bookList1.add(new Book("Mental Math", 1));
		bookList1.add(new Book("English Comprehension", 3));
		student1.setRequestedBooksList(bookList1);
		Tutor tempTutor = tutor1;
		Lesson lesson1 = new Lesson(student1, "English", "Available", tempTutor, sdf.parse("1-4-2015"), "Communication");
		tutor1.setLesson(lesson1);
		
		Lesson lesson2 = new Lesson(student1, "English", "Available", tempTutor, sdf.parse("1-4-2005"), "Skills");
		String description1 = lesson2.getDescription();
		
		// updating the lesson and testing
		
		tutor1.setLesson(lesson2, 1);

		assertEquals(description1, tutor1.getBookedLessons().get(0).getDescription());
	}

}
