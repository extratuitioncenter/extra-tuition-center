import java.text.SimpleDateFormat;
import java.util.ArrayList;

// utility class for filling the inital data
public class DataUtil {

	public static ArrayList<Tutor> getTutors() throws Exception {

		ArrayList<Tutor> tutorsList = new ArrayList<Tutor>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		// creating instances
		Tutor tutor1 = new Tutor("tutor 1");
		ArrayList<String> subs1 = new ArrayList<String>();
		subs1.add("english");
		tutor1.setSubjectList(subs1);

		Student student1 = new Student("Student 1", "Male", "1-1-1989", "George Street", "325611");
		student1.setMainTutor("tutor 1");
		ArrayList<Book> bookList1 = new ArrayList<Book>();
		bookList1.add(new Book("Mental Math", 1));
		bookList1.add(new Book("English Comprehension", 3));
		student1.setRequestedBooksList(bookList1);
		Tutor tempTut = tutor1;
		Lesson lesson1 = new Lesson(student1, "English", "Available", tempTut, sdf.parse("1-4-2015"), "Communication");
		tutor1.setLesson(lesson1);
		tutorsList.add(tutor1);

		Tutor tutor2 = new Tutor("tutor 2");
		ArrayList<String> subs2 = new ArrayList<String>();
		subs2.add("maths");
		tutor2.setSubjectList(subs2);

		Student student2 = new Student("Student 2", "Female", "1-1-1989", "William Street", "957441");
		student2.setMainTutor("tutor 2");
		ArrayList<Book> bookList2 = new ArrayList<Book>();
		bookList2.add(new Book("Mental Math", 1));
		bookList2.add(new Book("English Comprehension", 3));
		student2.setRequestedBooksList(bookList1);
		Tutor tempTut1 = tutor2;
		Lesson lesson2 = new Lesson(student2, "Maths", "Available", tempTut1, sdf.parse("5-4-2015"), "Basic Maths");
		tutor2.setLesson(lesson2);

		tutorsList.add(tutor2);

		Tutor tutor3 = new Tutor("tutor 3");
		ArrayList<String> subs3 = new ArrayList<String>();
		subs3.add("iot");
		tutor3.setSubjectList(subs3);

		Student student3 = new Student("Student 3", "Male", "1-12-2003", "John Street", "655448");
		student3.setMainTutor("tutor 3");
		ArrayList<Book> bookList3 = new ArrayList<Book>();
		bookList3.add(new Book("IOT", 1));
		bookList3.add(new Book("Programming Principles", 3));
		student3.setRequestedBooksList(bookList1);
		Tutor tempTut2 = tutor3;
		Lesson lesson3 = new Lesson(student3, "IOT", "Available", tempTut2, sdf.parse("5-4-2015"), "Basic IOT");
		tutor3.setLesson(lesson3);

		tutorsList.add(tutor3);

		return tutorsList;
	}

	public static ArrayList<Student> getStudents() throws Exception {

		ArrayList<Student> studentsList = new ArrayList<Student>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		Tutor tutor1 = new Tutor("tutor 1");
		ArrayList<String> subs1 = new ArrayList<String>();
		subs1.add("english");
		tutor1.setSubjectList(subs1);

		Student student1 = new Student("Student 1", "Male", "1-1-1989", "George Street", "325611");
		student1.setMainTutor("tutor 1");
		ArrayList<Book> bookList1 = new ArrayList<Book>();
		bookList1.add(new Book("Mental Math", 1));
		bookList1.add(new Book("English Comprehension", 3));
		student1.setRequestedBooksList(bookList1);
		Student tempStd = student1;
		Lesson lesson1 = new Lesson(tempStd, "English", "Available", tutor1, sdf.parse("1-4-2015"), "Communication");
		student1.setLesson(lesson1);

		studentsList.add(student1);

		Tutor tutor2 = new Tutor("tutor 2");
		ArrayList<String> subs2 = new ArrayList<String>();
		subs2.add("maths");
		tutor2.setSubjectList(subs2);

		Student student2 = new Student("Student 2", "Female", "1-1-1989", "William Street", "957441");
		student2.setMainTutor("tutor 2");
		ArrayList<Book> bookList2 = new ArrayList<Book>();
		bookList2.add(new Book("Mental Math", 1));
		bookList2.add(new Book("English Comprehension", 3));
		student2.setRequestedBooksList(bookList1);
		Student tempStd1 = student2;
		Lesson lesson2 = new Lesson(tempStd1, "Maths", "Available", tutor2, sdf.parse("5-4-2015"), "Basic Maths");
		student2.setLesson(lesson2);

		studentsList.add(student2);

		Tutor tutor3 = new Tutor("tutor 3");
		ArrayList<String> subs3 = new ArrayList<String>();
		subs3.add("iot");
		tutor3.setSubjectList(subs3);

		Student student3 = new Student("Student 3", "Male", "1-12-2003", "John Street", "655448");
		student3.setMainTutor("tutor 3");
		ArrayList<Book> bookList3 = new ArrayList<Book>();
		bookList3.add(new Book("IOT", 1));
		bookList3.add(new Book("Programming Principles", 3));
		student3.setRequestedBooksList(bookList1);
		Student tempStd2 = student3;
		Lesson lesson3 = new Lesson(tempStd2, "IOT", "Available", tutor3, sdf.parse("5-4-2015"), "Basic IOT");
		student3.setLesson(lesson3);

		studentsList.add(student3);

		Student student4 = new Student("Student 4", "Male", "1-12-1995", "Roe Street", "596465");
		student4.setMainTutor("tutor 3");
		studentsList.add(student4);

		Student student5 = new Student("Student 5", "Female", "16-4-1993", "Michel road", "496566");
		student5.setMainTutor("tutor 2");
		studentsList.add(student5);

		Student student6 = new Student("Student 6", "Male", "11-2-1992", "Michel road", "641855");
		student6.setMainTutor("tutor 1");
		studentsList.add(student6);

		return studentsList;
	}

}
