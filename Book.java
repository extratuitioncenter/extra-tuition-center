
public class Book {

	// variables

	private String bookName;
	private Integer count;

	// constructors
	public Book() {

	}

	public Book(String bookName, Integer count) {
		super();
		this.bookName = bookName;
		this.count = count;
	}

	// getters and setters
	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "Book [bookName=" + bookName + ", count=" + count + "]";
	}

}
