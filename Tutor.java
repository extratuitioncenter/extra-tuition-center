import java.util.ArrayList;

// class with tutor details
public class Tutor {

	// variables
	private String name;
	private ArrayList<String> subjectList = new ArrayList<String>();
	private ArrayList<Lesson> bookedLessons = new ArrayList<Lesson>();

	// constructors
	public Tutor() {

	}

	public Tutor(String name) {
		super();
		this.name = name;
	}

	// getters and setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<String> getSubjectList() {
		return subjectList;
	}

	public void setSubjectList(ArrayList<String> subjectList) {
		this.subjectList = subjectList;
	}

	public ArrayList<Lesson> getBookedLessons() {
		return bookedLessons;
	}

	public void setBookedLessons(ArrayList<Lesson> bookedLessons) {
		this.bookedLessons = bookedLessons;
	}

	public void setLesson(Lesson lesson) {
		this.bookedLessons.add(lesson);
	}

	@Override
	public String toString() {
		return "Tutor [name=" + name + ", subjectList=" + subjectList + ", bookedLessons=" + bookedLessons + "]";
	}

	public void setLesson(Lesson lesson, Integer updateChoice) {
		this.bookedLessons.set(updateChoice - 1, lesson);

	}

}
