import java.util.ArrayList;

// student class
public class Student {

	// variables
	private String name;
	private String gender;
	private String dob;
	private String address;
	private String phoneNumber;
	private String mainTutor;
	private String notes;

	// required lists
	private ArrayList<Lesson> lessonList = new ArrayList<Lesson>();
	private ArrayList<Book> requestedBooksList = new ArrayList<Book>();

	// constructors
	public Student() {

	}

	public Student(String name, String gender, String dob, String address, String phoneNumber) {
		super();
		this.name = name;
		this.gender = gender;
		this.dob = dob;
		this.address = address;
		this.phoneNumber = phoneNumber;
	}

	// getters and setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public ArrayList<Lesson> getLessonList() {
		return lessonList;
	}

	public void setLessonList(ArrayList<Lesson> lessonList) {
		this.lessonList = lessonList;
	}

	public ArrayList<Book> getRequestedBooksList() {
		return requestedBooksList;
	}

	public void setRequestedBooksList(ArrayList<Book> requestedBooksList) {
		this.requestedBooksList = requestedBooksList;
	}

	public String getMainTutor() {
		return mainTutor;
	}

	public void setMainTutor(String mainTutor) {
		this.mainTutor = mainTutor;
	}

	public void setLesson(Lesson lesson) {
		this.lessonList.add(lesson);
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", gender=" + gender + ", dob=" + dob + ", address=" + address
				+ ", phoneNumber=" + phoneNumber + ", mainTutor=" + mainTutor + ", lessonList=" + lessonList
				+ ", requestedBooksList=" + requestedBooksList + "]";
	}

	public void setLesson(Lesson lesson, Integer updateChoice) {
		this.lessonList.set(updateChoice - 1, lesson);
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	

}
