import java.util.Date;

// lesson class
public class Lesson {

	// variables
	private Student student;
	private String subject;
	private String status;
	private Tutor tutor;
	private Date date;
	private String description;

	// default constructor
	public Lesson() {

	}

	// parameterized constructor
	public Lesson(Student student, String subject, String status, Tutor tutor, Date date, String description) {
		super();
		this.student = student;
		this.subject = subject;
		this.status = status;
		this.tutor = tutor;
		this.date = date;
		this.description = description;
	}

	// getters and setters
	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Tutor getTutor() {
		return tutor;
	}

	public void setTutor(Tutor tutor) {
		this.tutor = tutor;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	// to string method
	@Override
	public String toString() {
		return "Lesson [student=" + student + ", subject=" + subject + ", status=" + status + ", tutor=" + tutor
				+ ", date=" + date + ", description=" + description + "]";
	}
	
	

}
